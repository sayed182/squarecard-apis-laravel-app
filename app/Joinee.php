<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joinee extends Model
{
    //
    protected $fillable = ['invitation_code'];
}
