<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //
    protected $fillable = ['image_path', 'basic_event_id', 'joinee_id', 'user_id'];
}
