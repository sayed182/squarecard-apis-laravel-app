<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueSchedule extends Model
{
    //
    public function getVenue($id){
        return $this->where('event_location_id',$id)->get();
    }
}
