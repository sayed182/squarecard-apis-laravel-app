<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Profile extends Model
{
    //
    protected $id;
    protected $fillable =['profile_image_path', 'profile_name', 'occupation', 'age', 'description'];
//    public function createNew($img_path, $name, $occupation = null, $age = null, $description = null){
//        $this->create
//    }

    public function stories(){
        return $this->hasMany('App\Story');
    }

    public function getEvents($id){

        return $this->find($id)->hasMany('App\BasicEvents')->get();
    }

    public function getStories($id){
        return $this->find($id)->hasMany('App\Story')->get();
    }

    public function getProfiles(){
        return $this->where('user_id', Auth::id())->get();
    }

}
