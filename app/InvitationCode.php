<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvitationCode extends Model
{
    //
    protected $table = "invitation_codes";
}
