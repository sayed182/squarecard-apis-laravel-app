<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    //
    public function register( Request $request ){
        $info = $request->validate([
            'name' => 'required|string',
            'password' => 'required|string|min:8',
            'email' => 'required|string|unique:users',
            'mobile_no' => 'required|integer',
        ]);
        $credentials = $request->only(['name', 'password', 'email', 'mobile_no']);
        if(!$info){
            return response("feild empty",404);
        }
        $credentials["password"] = Hash::make($request->password);
        $user = User::create($credentials);

        return response($user);
    }
}
