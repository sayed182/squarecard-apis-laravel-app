<?php

namespace App\Http\Controllers\Api\v1;

use App\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ImageController extends Controller
{
    //

    public function success($data)
    {
        return response([
            "message" => 'success',
            'data' => $data
        ],200);
    }
    public function failed($data)
    {
        return response([
            "message" => 'fail',
            'data' => $data
        ],400);
    }

    public function getImages(Request $request){
        $event_id = $request->event_id;
        if($request->has('user_id')){
            $paths = Gallery::where('user_id', $request->user_id)->where('basic_event_id', $event_id)->get();
        }elseif ($request->has('joinee_id')){
            $paths = Gallery::where('joinee_id', $request->joinee_id)->where('basic_event_id', $event_id)->get();
        }
    }
    public function storeImage(Request $request){
        if(!$request->has('user_id')) {
            $request->validate([
                'event_id' => 'required',
                'joinee_id' =>  'required',
            ]);
            $data['user_id'] = $request->joinee_id;
        }else {
            $request->validate([
                'event_id' => 'required',
                'user_id' => 'required',
            ]);
            $data['user_id'] = $request->user_id;
        }
        $data['basic_event_id'] = $request->event_id;
        $data['image_path'] = "";
        $files = $request->file('photos');
        foreach($files as $key => $file){
            $data['image_path'] = $file->storePublicly('public');
            try{
                $d = Gallery::create($data);
                return $this->success($d);
            }catch (Exception $e){
                return $this->failed($e);
            }

        }
        return response($data);
    }

    public function deleteImage(Request $request){
        $imageId = $request->id;
        $isUser = Gallery::find($imageId)->user_id;
        return response(Auth::id());
        if($isUser != null && !$request->has('user_id')){
           return $this->failed("Not authorized to perform this action");
        }
        $gallery = new Gallery();
//        if($gallery->find($imageId)->delete()){
//            return $this->success("Success fully deleted");
//        }
    }
}
