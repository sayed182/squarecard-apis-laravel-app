<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    public function login( Request $request) {
       $request->validate([
            'email'=> 'required|email',
            'password' => 'required|string',
        ]);
        $credentials = $request->only('email','password');
//        return response(Auth::user());
        if(!Auth::attempt($credentials)){
            return response(
                "Authentication Unsuccessfull",
                400
            );
        }
        $token = Auth::user()->createToken("authToken")->accessToken;
        return response(["user"=>Auth::user(), "access_token"=>$token],200);
    }
}
