<?php

namespace App\Http\Controllers\Api\v1;

use App\BasicEvents;
use App\EventLocation;
use App\Gallery;
use App\Http\Controllers\Controller;
use App\InvitationCode;
use App\Joinee;
use App\Profile;
use App\Story;
use App\User;
use App\VenueSchedule;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Image;
use phpDocumentor\Reflection\Location;

class MasterController extends Controller
{
    //
    protected $user_id;
    public function __construct(){
        $this->user_id = Auth::id();
    }

    public function success($data)
    {
        return response([
            "message" => 'success',
            'data' => $data
        ],200);
    }
    public function failed($data)
    {
        return response([
            "message" => 'fail',
            'data' => $data
        ],400);
    }

//    -------------------- Get Functions ---------------------------- //

    public function getAllProfiles(){
        $user = new Profile;
        $profiles = $user->getProfiles();
        if($profiles){
            return response([
                'message' => 'success',
                'data' => $profiles
            ], 200);
        }else{
            return response(['message' => 'No matching profiles found for this user'], 404);
        }
    }

    public function getProfile(Request $request){
        $request->validate([
            'profile_id' => 'required',
        ]);
        $profile = Profile::where('id', $request->profile_id)->where('user_id', Auth::id())->first();
        if($profile){
            return response([
                'message' => 'success',
                'data' => $profile
            ], 200);
        }else{
            return response(['message' => 'No matching profile found.'], 404);
        }
    }

    public function getAllEvents(Request $request){
        $request->validate(['profile_id' => 'required']);
        $user = new Profile();
        $events = $user->getEvents($request->profile_id);
        if($events){
            return response([
                'message' => 'success',
                'data' => $events
            ], 200);
        }else{
            return response(['message' => 'No matching events found for this user'], 404);
        }
    }

    public function getEvent(Request $request){
        $request->validate([
            'event_id' => 'required',
            'profile_id' => 'required',
        ]);
        $event = BasicEvents::where('id', $request->event_id)
                              ->where('profile_id', $request->profile_id)
                              ->where('user_id', Auth::id());
        if($event){
            return response($event,200);
        }else{
            return response('No matching events found for this user', 404);
        }
    }

    public function getAllStories(Request $request){
        $request->validate(['profile_id' => 'required']);
        $profile_id = $request->profile_id;
        $user = new Profile();
        $stories = $user->getStories($profile_id);
        if($stories){
            return response([
                'message' => 'success',
                'data' => $stories
            ], 200);
        }else{
            return response(['message' => 'No matching stories found for this user'], 404);
        }
    }

    public function getStory(Request $request){
        $request->validate(['id' => 'required']);
        $id = $request->id;
        $user = new Profile();
        $stories = Story::find($id);
        if($stories){
            return response([
                'message' => 'success',
                'data' => $stories
            ], 200);
        }else{
            return response(['message' => 'No matching stories found for this user'], 404);
        }
    }

    public function getLocation(Request $request){
        $request->validate([
           'event_id' => 'required',
           'profile_id' => 'required'
        ]);
        $location = Location::where('event_id', $request->event_id)->where('profile_id', $request->profile_id)->get();
        if($location){
            return response([
                'message' => 'success',
                'data' => $location
            ], 200);
        }else{
            return response(['message' => 'No matching location found for this user'], 404);
        }
    }

    public function getAllVenues(Request $request){
        $request->validate(['location_id' => 'required']);
        $venue = VenueSchedule::where('event_location_id', $request->location_id)->get();
        $location = EventLocation::find($request->location_id)->venue_display_name;
        if($venue){
            return response([
                'message' => 'success',
                'data' => $venue,
            ],200);
        }else{
            return response([
                'message' => 'No venue found matching '.$location,
            ],404);
        }
    }

    public function getVenue(Request $request){
        $venue = VenueSchedule::find($request->id);
        if($venue){
            return response([
                'message' => 'success',
                'data' => $venue,
            ],200);
        }else{
            return response([
                'message' => 'No data found.',
            ],404);
        }
    }


    public function getAllImages(Request $request){
        $images = Gallery::where('user_id', Auth::id());
        if($images){
            return response([
                "message" => "success",
                "data" => $images,
            ], 200);
        }else{
            return response([
                "message" => 'No data found matching user',
            ],404);
        }
    }


//    ------------------------ Create Functions ---------------------------- //

    public function createProfile(Request $request){
        $data = $request->only(['profile_name', 'occupation', 'age', 'description']);
        $validator = $request->validate( [
            'profile_name' => 'required|string',
            'profile_image' => 'required|image',
            'occupation' => 'string',
            'age' => 'date',
        ]);
        $data['user_id'] = Auth::id();
        $data['profile_image_path'] = $request->profile_image->store('photos');
        Profile::unguard();
        try{
            $d = Profile::create($data);
            return $this->success($d);
        }catch (Exception $e){
            return $this->failed($e);
        }
        Profile::reguard();
    }

    public function createEvent(Request $request){
        $data = $request->only(['event_type_id', 'event_name', 'start_date', 'end_date', 'note', 'profile_id']);
        $validator = $request->validate([
            'event_type_id' => 'required|numeric',
            'event_name' => 'required|string',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'profile_id' => 'required|numeric'
        ]);
        $data['user_id'] = Auth::id();
        BasicEvents::unguard();
        try{
            $d = BasicEvents::create($data);
            return $this->success($d);
        }catch (Exception $e){
            return $this->failed($e);
        }
        BasicEvents::reguard();
    }

    public function createStory(Request $request){
        $request->validate([
            'event_id' => 'required',
            'profile_id' => 'required',
            'story_title' => 'required'
        ]);
        $data = $request->only(['story_title', 'story_description', 'profile_id']);
        $data['basic_event_id'] = $request->event_id;
        Story::unguard();
        try{
            $d = Story::create($data);
            return $this->success($d);
        }catch (Exception $e){
            return $this->failed($e);
        }
    }

    public function createLocation(Request $request){
        $data = $request->only(['geo_location', 'flat_no', 'landmark', 'city', 'venue_display_name', 'event_id']);
        $validator = $request->validate([
            'geo_location' => 'string',
            'flat_no' => 'string',
            'landmark'=> 'string',
            'city' => 'string',
            'venue_display_name' => 'string',
            'event_id' => 'numeric|required',
        ]);
        $data['user_id'] = Auth::id();
        EventLocation::unguard();
        EventLocation::create($data);
        EventLocation::reguard();
    }
    public function createVenue(Request $request){
        $data = $request->only(['event_name', 'event_date', 'start_time', 'end_time', 'note', 'event_location_id']);
        $validator = $request->validate([
            'event_name' => 'string|required',
            'event_date' => 'date|required',
            'start_time' => 'required',
            'end_time' => 'required',
            'event_location_id' => 'required|numeric'
        ]);
        VenueSchedule::unguard();
        try{
            $d = VenueSchedule::create($data);
            return $this->success($d);
        }catch (Exception $e){
            return $this->failed($e);
        }

        VenueSchedule::reguard();


    }

    public function createInvitation(Request $request){
        $code = uniqid();
        $request->validate([
            'event_id' => 'required'
        ]);
        $code->validate([
           'invitation_code' => 'unique:invitation_codes'
        ]);
        $data = $request->only(['event_id', 'qr_code']);
        $data['invitation_code'] = $code;
        $data['user_id'] = Auth::id();
        InvitationCode::unguard();
        try{
            $d = InvitationCode::create($data);
            return $this->success($d);
        }catch (Exception $e){
            return $this->failed($e);
        }
        InvitationCode::reguard();
    }

    public function createJoinee(Request $request){
        $request->validate([
            'invitation_code' => 'required'
        ]);
        $data = $request->only(['invitation_code']);
        try{
            $joinee = Joinee::create($data);
            return $this->success($joinee);
        }catch(Exception $e){
            return $this->failed($e);
        }
    }


//    -------------------- Update Functions ---------------------------- //

    public function updateProfile(Request $request){
        return response($request->profile_id);
        $data = $request->only(['profile_image_path', 'profile_name', 'occupation', 'age', 'description']);
        $profile_id = $request->id;
        $validator = $request->validate( [
            'profile_id' => 'required',
            'profile_name' => 'string|min:1',
            'profile_image_path' => 'image',
            'occupation' => 'string',
            'age' => 'date',
        ]);
        $profile = new Profile();
        try{
            $profile->where('id', $profile_id)->update($data);
            return $this->success($profile);
        }catch(Exception $e){
            return $this->failed($e);
        }
    }
    public function updateEvent(Request $request){
        $data = $request->only(['event_type_id', 'event_name', 'start_date', 'end_date', 'note']);
        $validator = $request->validate([
            'id' => 'required',
            'event_type_id' => 'numeric',
            'event_name' => 'string',
            'start_date' => 'date',
            'end_date' => 'date',
        ]);
        try{
            $event = BasicEvents::where('id', $request->id )->update($data);
            return $this->success($event);
        }catch (Exception $e){
            return $this->failed($e);
        }
    }
    public function updateLocation(Request $request){
        $data = $request->only(['geo_location', 'flat_no', 'landmark', 'city','event_id']);
        $validator = $request->validate([
            'id' => 'required',
            'geo_location' => 'string',
            'flat_no' => 'string',
            'landmark'=> 'string',
            'city' => 'string',
            'event_id' => 'numeric',
        ]);

        try{
            $location = EventLocation::where('id', $request->id)->update($data);
            return $this->success($location);
        }catch(Exception $e){
            return $this->failed($e);
        }
    }
    public function updateVenue(Request $request){
        $data = $request->only(['event_name', 'event_date', 'start_time', 'end_time', 'note', 'event_location_id']);
        $validator = $request->validate([
            'id' => 'required',
            'event_name' => 'string',
            'event_date' => 'date',
            'start_time' => 'required',
            'end_time' => 'required',
            'event_location_id' => 'required|numeric'
        ]);
        try{
            $venue = VenueSchedule::where('id', $request->id)->update($data);
            return $this->success($venue);
        }catch(Exception $e){
            return $this->failed($e);
        }
    }
    public function updateStory(Request $request){
        $request->validate([
            'id' => 'required',
            'basic_event_id' => 'numeric',
            'profile_id' => 'numeric',
            'story_title' => 'string|min:1'
        ]);
        $data = $request->only(['story_title', 'story_description', 'basic_event_id', 'profile_id']);

        try{
            $venue = Story::where('id', $request->id)->update($data);
            return $this->success($venue);
        }catch(Exception $e){
            return $this->failed($e);
        }
    }
    public function updateInvitation(Request $request){
        $request->validate(['id' => 'required']);
        try {
            $invite = InvitationCode::where('id', $request->id)->update([
                'invitation_code' => uniqid(),
            ]);
        }catch(Exception $e) {
            return $this->failed($e);
        }
    }
    public function updateJoineeEventCode(Request $request){
        $request->validate([
            'id' => 'required'
        ]);
        $data = $request->only(['invite', 'notification_access_token']);
        $joinee_id = $request->id;

        try{
            $joinee = Joinee::where('id', $joinee_id)->update($data);
            return $this->success($joinee);
        }catch(Exception $e){
            return $this->failed($e);
        }
    }

//   ------------------------ Delete Functions ----------------------//


    public function deleteProfile(Request $request){
        $request->validate([
           'profile_id' => 'required',
        ]);
        $profileId = $request->profile_id;
        try {
            $profile = Profile::find($profileId)->delete();
            return $this->success($profile);
        }catch (Exception $e){
            return $this->failed($e);
        }

    }
    public function deleteEvent(Request $request){
        $eventId = $request->id;
        try {
            $event = BasicEvents::find($eventId)->delete();
            return $this->success($event);
        }catch (Exception $e){
            return $this->failed($e);
        }
    }
    public function deleteLocation(Request $request){
        $id = $request->id;
        try {
            $location = new EventLocation();
            $location->where('id', $id)->delete();
            return $this->success($location);
        }catch (Exception $e) {
            return $this->failed($e);
        }
    }
    public function deleteVenue(Request $request){
        $id = $request->id;
        try {
            $venue = new VenueSchedule();
            $venue->where('id', $id)->delete();
            return $this->success($venue);
        }catch (Exception $e) {
            return $this->failed($e);
        }
    }
    public function deleteStory(Request $request){
        $id = $request->id;
        try {
            $story = new Story();
            $story->where('id', $id)->delete();
            return $this->success($story);
        }catch (Exception $e) {
            return $this->failed($e);
        }
    }
    public function deleteInvitation(Request $request){
        $id = $request->id;
        try {
            $invitaiton = new InvitationCode();
            $invitaiton->where('id', $id)->delete();
            return $this->success($invitaiton);
        }catch (Exception $e) {
            return $this->failed($e);
        }
    }
    public function deleteJoinee(Request $request){
        $id = $request->id;
        try {
            $joinee = new Joinee();
            $joinee->where('id', $id)->delete();
            return $this->success($joinee);
        }catch (Exception $e) {
            return $this->failed($e);
        }
    }



//  ------------------------ Helper Functions -------------------------//


}//End of class
