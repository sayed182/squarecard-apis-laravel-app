#SquareCard API Documentation
### API Route List

- #### **/login**
- #### **/register**
- #### **/profile**
| Sub link          | Feilds accepted | Data type accepted | Required fields  | Description |
| :------------:| :----: | :---: | :-----:| :-----: |
| / | profile_id | number | **profile_id** | Gets a single profile on the basis of the provided Profile ID as request data |
| /all      | --- | --- | --- | Fetches all the available profile linked to the registered user ID |
| /create      | profile_name, profile_image, occupation, age | string, image, string, datetime | **profile_name**, **profile_image** | Creates a profile with the request data.
| /update      | 'profile_id', 'profile_image_path', 'profile_name', 'occupation', 'age', 'description' | number, image, string, string, datetime, string,  |  **profile_id** | Updates a profile |
| /delete      | profile_id | number |  **profile_id** | Deletes a profile from the given profile_id |

 
- #### **/events**

| Sub link          | Feilds accepted | Data type accepted | Required fields  | Description |
| :------------:| :----: | :---: | :-----:| :-----: |
| / | profile_id | number | **profile_id** | Gets a single profile on the basis of the provided Profile ID as request data |
| /all      | --- | --- | --- | Fetches all the available profile linked to the registered user ID |
| /create      | profile_name, profile_image, occupation, age | string, image, string, datetime | **profile_name**, **profile_image** | Creates a profile with the request data.
| /update      | 'profile_id', 'profile_image_path', 'profile_name', 'occupation', 'age', 'description' | number, image, string, string, datetime, string,  |  **profile_id** | Updates a profile |
| /delete      | profile_id | number |  **profile_id** | Deletes a profile from the given profile_id |


- #### **/story**

| Sub link          | Feilds accepted | Data type accepted | Required fields  | Description |
| :------------:| :----: | :---: | :-----:| :-----: |
| / | profile_id | number | **profile_id** | Gets a single profile on the basis of the provided Profile ID as request data |
| /all      | --- | --- | --- | Fetches all the available profile linked to the registered user ID |
| /create      | profile_name, profile_image, occupation, age | string, image, string, datetime | **profile_name**, **profile_image** | Creates a profile with the request data.
| /update      | 'profile_id', 'profile_image_path', 'profile_name', 'occupation', 'age', 'description' | number, image, string, string, datetime, string,  |  **profile_id** | Updates a profile |
| /delete      | profile_id | number |  **profile_id** | Deletes a profile from the given profile_id |


- #### **/location**

| Sub link          | Feilds accepted | Data type accepted | Required fields  | Description |
| :------------:| :----: | :---: | :-----:| :-----: |
| / | profile_id | number | **profile_id** | Gets a single profile on the basis of the provided Profile ID as request data |
| /all      | --- | --- | --- | Fetches all the available profile linked to the registered user ID |
| /create      | profile_name, profile_image, occupation, age | string, image, string, datetime | **profile_name**, **profile_image** | Creates a profile with the request data.
| /update      | 'profile_id', 'profile_image_path', 'profile_name', 'occupation', 'age', 'description' | number, image, string, string, datetime, string,  |  **profile_id** | Updates a profile |
| /delete      | profile_id | number |  **profile_id** | Deletes a profile from the given profile_id |


- #### **/venue**

| Sub link          | Feilds accepted | Data type accepted | Required fields  | Description |
| :------------:| :----: | :---: | :-----:| :-----: |
| / | profile_id | number | **profile_id** | Gets a single profile on the basis of the provided Profile ID as request data |
| /all      | --- | --- | --- | Fetches all the available profile linked to the registered user ID |
| /create      | profile_name, profile_image, occupation, age | string, image, string, datetime | **profile_name**, **profile_image** | Creates a profile with the request data.
| /update      | 'profile_id', 'profile_image_path', 'profile_name', 'occupation', 'age', 'description' | number, image, string, string, datetime, string,  |  **profile_id** | Updates a profile |
| /delete      | profile_id | number |  **profile_id** | Deletes a profile from the given profile_id |


- #### **/createInvitation**

| Sub link          | Feilds accepted | Data type accepted | Required fields  | Description |
| :------------:| :----: | :---: | :-----:| :-----: |
| / | profile_id | number | **profile_id** | Gets a single profile on the basis of the provided Profile ID as request data |
| /all      | --- | --- | --- | Fetches all the available profile linked to the registered user ID |
| /create      | profile_name, profile_image, occupation, age | string, image, string, datetime | **profile_name**, **profile_image** | Creates a profile with the request data.
| /update      | 'profile_id', 'profile_image_path', 'profile_name', 'occupation', 'age', 'description' | number, image, string, string, datetime, string,  |  **profile_id** | Updates a profile |
| /delete      | profile_id | number |  **profile_id** | Deletes a profile from the given profile_id |


- #### **/getImages**
- #### **/storeImages**
- #### **/deleteImage**
