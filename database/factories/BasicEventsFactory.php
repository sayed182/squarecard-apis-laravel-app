<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\BasicEvents::class, function (Faker $faker) {
    $start_date = Carbon::now()->subDays(random_int(5,10));
    $end_date = Carbon::now();
    $event = json_decode(json_encode(App\TypeOfEvent::all()->pluck('id')), true);
    return [
        //
        "event_name" => $faker->word,
        "start_date" => $start_date,
        "end_date" => $end_date,
        "note" => $faker->paragraphs($nb=3, $asText=true),
        "event_type_id" => $faker->randomElement($event),
        "profile_id" => random_int(1,5),
        "user_id" => random_int(1,5)
    ];
});
