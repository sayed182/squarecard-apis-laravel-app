<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
        //
        "profile_image_path" => $faker->imageUrl($width=400, $height=400),
        "profile_name" => $faker->name,
        "occupation" => $faker->jobTitle,
        "age" => Carbon::now()->subYears(random_int(20,75)),
        "description" => $faker->paragraphs($nb=3, $asText=true),
        "user_id" => random_int(1,5),
    ];
});
