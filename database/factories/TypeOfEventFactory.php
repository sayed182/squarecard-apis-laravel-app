<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\TypeOfEvent::class, function (Faker $faker) {
    return [
        //
        "name" => $faker->randomElement(array('Wedding', 'Reception', 'Birthday', 'Baby Shower'))
    ];
});
