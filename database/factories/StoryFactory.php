<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Story::class, function (Faker $faker) {
    $profile = json_decode(json_encode(App\Profile::all()->pluck('id')),true);
    return [
        "story_title" => $faker->sentence($nbWords=10),
        "story_description" => $faker->paragraphs($nb=3, $asText=true),
        "basic_event_id" => random_int(1,5),
        "profile_id" => $faker->randomElement($profile),
        "user_id" => random_int(1,5)
    ];
});
