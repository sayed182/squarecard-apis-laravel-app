<?php

use Illuminate\Database\Seeder;

class BasicEventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\BasicEvents::class, 10)->create();
        factory(App\Story::class,20)->create();
    }
}
