<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEventForeignToLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_locations', function (Blueprint $table) {
            //
            $table->bigInteger('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('basic_events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location', function (Blueprint $table) {
            //
            $table->dropForeign('event_id');
            $table->dropColumn('event_id');
        });
    }
}
