<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasicEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_events', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("event_type_id")->unsigned();
            $table->string("event_name");
            $table->date("start_date");
            $table->date("end_date");
            $table->text("note")->nullable();
            $table->foreign("event_type_id")->references("id")->on("type_of_events");
            $table->foreignId("profile_id")->constrained()->onDelete("cascade");
            $table->foreignId("user_id")->constrained()->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basic_events');
    }
}
