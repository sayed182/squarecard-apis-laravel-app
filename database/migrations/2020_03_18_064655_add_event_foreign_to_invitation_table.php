<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEventForeignToInvitationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invitation_codes', function (Blueprint $table) {
            //
            $table->bigInteger('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('basic_events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invitation', function (Blueprint $table) {
            //
            $table->dropColumn('event_id');
        });
    }
}
