<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVenueSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venue_schedules', function (Blueprint $table) {
            $table->id();
            $table->string("event_name");
            $table->date("event_date");
            $table->time("start_time");
            $table->time("end_time");
            $table->text("note")->nullable();
            $table->foreignId("event_location_id")->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venue_schedules');
    }
}
