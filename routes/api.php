<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::Post('/storeImages', 'Api\v1\ImageController@storeImage');
Route::Post('/deleteImage', 'Api\v1\ImageController@deleteImage');
Route::get('/test', function (){
    return response('working');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'Api\v1\RegisterController@register');
Route::post('/login', 'Api\v1\LoginController@login');

Route::middleware('auth:api')->group(function (){

    Route::prefix('profile')->group(function(){
        Route::post('/','Api\v1\MasterController@getProfile');
        Route::post('/all', 'Api\v1\MasterController@getAllProfiles');
        Route::post('/create', 'Api\v1\MasterController@createProfile');
        Route::post('/update', 'Api\v1\MasterController@updateProfile');
        Route::post('/delete', 'Api\v1\MasterController@deleteProfile');
    });

    Route::prefix('events')->group(function(){
        Route::post('/','Api\v1\MasterController@getEvent');
        Route::post('/all ', 'Api\v1\MasterController@getAllEvents');
        Route::post('/create ', 'Api\v1\MasterController@createEvent');
        Route::post('/update', 'Api\v1\MasterController@updateEvent');
        Route::post('/delete', 'Api\v1\MasterController@deleteEvent');
    });

    Route::prefix('story')->group(function(){
        Route::post('/','Api\v1\MasterController@getStory');
        Route::post('/all ', 'Api\v1\MasterController@getAllStories');
        Route::post('/create ', 'Api\v1\MasterController@createStory');
        Route::post('/update', 'Api\v1\MasterController@updateStory');
        Route::post('/delete', 'Api\v1\MasterController@deleteStory');
    });

    Route::prefix('location')->group(function(){
        Route::post('/','Api\v1\MasterController@getLocation');
        Route::post('/all ', 'Api\v1\MasterController@getAllLocations');
        Route::post('/create ', 'Api\v1\MasterController@createLocation');
        Route::post('/update', 'Api\v1\MasterController@updateLocation');
        Route::post('/delete', 'Api\v1\MasterController@deleteLocation');
    });

    Route::prefix('venue')->group(function(){
        Route::post('/','Api\v1\MasterController@getVenue');
        Route::post('/all ', 'Api\v1\MasterController@getAllVenues');
        Route::post('/create ', 'Api\v1\MasterController@createVenue');
        Route::post('/update', 'Api\v1\MasterController@updateVenue');
        Route::post('/delete', 'Api\v1\MasterController@deleteVenue');
    });

    Route::Post('/createInvitation', 'Api\v1\MasterController@createInvitation');

//    Handling Image Requests......
    Route::Post('/getImages', 'Api\v1\ImageController@getAllImages');




});
